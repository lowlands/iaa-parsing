IAA-parsing
===========

## Introduction

This package contains the software and data used in "Learning to parse
with IAA-weighted loss" by Martinez Alonso et al. (2015). If you use
this package, please cite:

````
@inproceedings{Alonso:ea:2015:NAACL,
	Author = {Hector Martinez Alonso, Barbara Plank, Arne Skjærholt and Anders Søgaard},
	Booktitle = {NAACL},
	Title = {Learning to parse with IAA-weighted loss},
	Address = {Denver},
	Year = {2015}}
````



## Data

